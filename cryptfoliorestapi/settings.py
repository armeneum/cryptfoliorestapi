import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = '=w*(3k#!@!n1(^wmm67z8jhtzvg7^=29sk4%s%&0k#zcf2&&(5'

DEBUG = True

ALLOWED_HOSTS = ['cryptfoliorestapi.herokuapp.com','127.0.0.1','localhost']

INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'restapiapp.apps.RestapiappConfig',
	'rest_framework',
	'rest_framework.authtoken',
	'corsheaders',
]

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'whitenoise.middleware.WhiteNoiseMiddleware',	
	'django.contrib.sessions.middleware.SessionMiddleware',
	'corsheaders.middleware.CorsMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	#'restapiapp.middleware.RequestResponseLoggingMiddleware',
]

ROOT_URLCONF = 'cryptfoliorestapi.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
			],
		},
	},
]

WSGI_APPLICATION = 'cryptfoliorestapi.wsgi.application'

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'cryptfolio',
		'USER': 'aris',
		'PASSWORD': 'Password_123',
		'HOST': 'localhost',
		'PORT': '',
	}
}

# Heroku: Update database configuration from $DATABASE_URL.
import dj_database_url
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

AUTH_PASSWORD_VALIDATORS = [
	{
		'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
		'OPTIONS': {
			'min_length': 8
		}
	},
	{
		'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	},
	{
		'NAME': 'restapiapp.validators.RegexPasswordValidator',
		'OPTIONS': {
			'pattern': r'^[\w!#$%&? ]*$',
			'message': 'Password must not contain invalid characters',
		}
	},
	{
		'NAME': 'restapiapp.validators.RegexPasswordValidator',
		'OPTIONS': {
			'pattern': r'.*[a-z]',
			'message': 'Password must contain at least one lowercase character',
		}
	},
	{
		'NAME': 'restapiapp.validators.RegexPasswordValidator',
		'OPTIONS': {
			'pattern': r'.*[A-Z]',
			'message': 'Password must contain at least one uppercase character',
		}
	},
	{
		'NAME': 'restapiapp.validators.RegexPasswordValidator',
		'OPTIONS': {
			'pattern': r'.*[0-9]',
			'message': 'Password must contain at least one number',
		}
	},
	{
		'NAME': 'restapiapp.validators.RegexPasswordValidator',
		'OPTIONS': {
			'pattern': r'.*[_!#$%&? \"]',
			'message': 'Password must contain at least one special character',
		}
	},
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

STATIC_ROOT = os.path.join(PROJECT_ROOT)

STATIC_URL = '/static/'

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

REST_FRAMEWORK = {
	'DEFAULT_PERMISSION_CLASSES': [
		'rest_framework.permissions.IsAuthenticated',
	],
	'DEFAULT_AUTHENTICATION_CLASSES': [
		'rest_framework.authentication.TokenAuthentication',
	]
}

AUTH_USER_MODEL = 'restapiapp.User'

CORS_ORIGIN_WHITELIST = (
	'localhost:3000',
)