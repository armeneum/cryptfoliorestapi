from django.conf.urls import include
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path
from . import views
from .views import AuthToken

urlpatterns = [
	path('', include('restapiapp.urls')),
	path('admin/',admin.site.urls),
    path('get_auth_token/', AuthToken.as_view(), name='get_auth_token'),
]