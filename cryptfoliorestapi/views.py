from restapiapp.models import User
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

class AuthToken(ObtainAuthToken):

	def post(self, request, *args, **kwargs):
		user = User.objects.get(username=request.data["username"])
		if user.check_password(request.data["password"]):	
			token = Token.objects.get(user=user)	
			return Response({
				'token': token.key,
			})
		else:
			return Response(data={
				"Authentication Error": "Email or Password is Incorrect"
			},
			status=500)