import re
from django.core.exceptions import ValidationError

class RegexPasswordValidator:
	def __init__(self, pattern="^.*$", message="Invalid Password"):
		self.pattern = pattern
		self.message = message

	def validate(self, password, user=None):
		if not re.match(self.pattern,password):
			raise ValidationError(
				self.message,
				code='password_invalid',
				params={
					'pattern': self.pattern,
					'message': self.message,
				}
			)

	def get_help_text(self):
		return "Password does not match pattern : {}".format(self.pattern)