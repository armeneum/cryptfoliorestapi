from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from collections import defaultdict
from .serializers import *
from .models import *

""" Basic Views """

class CoinViewSet(ModelViewSet):
	serializer_class = CoinSerializer
	queryset = Coin.objects.all()
	
	@action(methods=['get'],detail=False)
	def mycoins(self, request):
		token = self.request.auth
		queryset = Coin.objects.filter(coinholding__user=token.user)
		serializer = self.serializer_class(queryset, many=True)
		return Response(serializer.data)

class CoinHoldingViewSet(ModelViewSet):
	serializer_class = CoinHoldingSerializer
	queryset = CoinHolding.objects.none()

	def get_queryset(self):
		token = self.request.auth
		return token.user.coinholding_set

class PortfolioViewSet(ReadOnlyModelViewSet):
	serializer_class = PortfolioSerializer
	queryset = Portfolio.objects.none()

	def restructurePortfolio(self, serializer):
		data = {}
		for coinholding in serializer.data["coinholding_set"]:
			data[coinholding["coin"]["name"]] = coinholding
		return data

	def get_queryset(self):
		token = self.request.auth
		return [token.user.portfolio]

	@action(methods=['get'], detail=False)
	def myportfolio(self, request):
		token = self.request.auth
		portfolio = Portfolio.objects.get(user_id=token.user_id)
		serializer = self.serializer_class(portfolio)
		data = self.restructurePortfolio(serializer)
		return Response(data)


class UserViewSet(ModelViewSet):
	serializer_class = UserSerializer
	queryset = User.objects.none()

	def get_permissions(self):
		if self.action == 'create':
			permissions_classes = [AllowAny]
		else:
			permissions_classes = [IsAuthenticated]
		return [permission() for permission in permissions_classes]

	def get_queryset(self):
		token = self.request.auth
		return [token.user]

class CoinPriceViewSet(ReadOnlyModelViewSet):
	serializer_class = CoinPriceSerializer
	queryset = CoinPrice.objects.all()

	def get_all(data=None):
		return CoinPrice.objects.all(
			).order_by(
			'coin','-date','-time'
		)

	def lastPriceBefore(date,time,coin):
		return CoinPriceViewSet.get_all()[0]

	def restructureCoinPrices(self,serializer):
		data = defaultdict(list)
		for coinData in serializer.data:
			coinName = coinData["coin"]["name"]
			del coinData["coin"]
			data[coinName].append(coinData)
		return data

	def list(self, request):
		queryset = CoinPriceViewSet.get_all()
		serializer = self.serializer_class(queryset, many=True)
		data = self.restructureCoinPrices(serializer)
		return Response(data)

	def retrieve(self, request, pk=None):
		self.serializer_class = CoinPriceDetailSerializer
		results = CoinPrice.objects.filter(coin__name=pk)
		serializer = self.serializer_class(results, many=True)
		return Response(serializer.data)

	@action(methods=['get'], detail=False)
	def mycoinprices(self, request):
		token = self.request.auth
		queryset = CoinPrice.objects.filter(
			coin__coinholding__user=token.user
		).order_by(
			'coin','-date','-time'
		)
		serializer = self.serializer_class(queryset, many=True)
		data = self.restructureCoinPrices(serializer)
		return Response(data)

class TradeViewSet(ModelViewSet):
	serializer_class = TradeSerializer
	queryset = Trade.objects.none()

	def get_queryset(self):
		token = self.request.auth
		return token.user.trade_set

	def create(self,request):	
		# newRequest = request.
		# try:
		# 	date = request.data['date']
		# 	time = request.data['time']
		# 	buyCoin = request.data['buyCoin']
		# 	sellCoin = request.data['sellCoin']
		# 	newRequest.data = {
		# 		**request.data,
		# 		'buyCoin': Coin.objects.get(name=buyCoin.lower()),
		# 		'buyPrice': CoinPriceViewSet.lastPriceBefore(date,time,buyCoin),
		# 		'sellCoin': Coin.objects.get(name=sellCoin.lower()),
		# 		'sellPrice': CoinPriceViewSet.lastPriceBefore(date,time,sellCoin)
		# 	}
		# except Exception as e:
		# 	raise e
		# 	pass
		super().create(newRequest)
	# def create(self,request):
	# 	# while True:
	# 	# 	print(eval(input()))
	# 	if serializer.is_valid()
	# 		date = request.data['date']
	# 		time = request.data['time']
	# 		buyCoin = Coin.objects.get(name=request.data['buyCoin'].lower())
	# 		buyPrice = CoinPriceViewSet.lastPriceBefore(date,time,buyCoin)
	# 		sellCoin = Coin.objects.get(name=request.data['sellCoin'].lower())
	# 		sellPrice = CoinPriceViewSet.lastPriceBefore(date,time,sellCoin)
	# 		user = request.auth.user
	# 		reformattedData = {
	# 			**request.data,
	# 			'buyCoin': buyCoin,
	# 			'buyPrice': buyPrice,
	# 			'sellCoin': sellCoin,
	# 			'sellPrice': sellPrice,
	# 			'user': user
	# 		}
	# 		print(reformattedData)
	# 		Trade.objects.create(**reformattedData)
	# 	else:
	# 		print(dir(e))
	# 		return Response({
	# 			"Success": False,
	# 			"Error": e
	# 		})
	# 	return Response({"Success": True})

class TransferViewSet(ModelViewSet):
	serializer_class = TransferSerializer
	queryset = Transfer.objects.none()

	def get_queryset(self):
		token = self.request.auth
		return token.user.transfer_set