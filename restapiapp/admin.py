from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *
import inspect,sys

admin.site.register(User, UserAdmin)
admin.site.register(Coin)
admin.site.register(Portfolio)
admin.site.register(CoinHolding)
admin.site.register(CoinPrice)
admin.site.register(Trade)
admin.site.register(Transfer)