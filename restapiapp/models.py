from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinLengthValidator, MaxLengthValidator, RegexValidator, DecimalValidator
from django.core.exceptions import ValidationError
from rest_framework.authtoken.models import Token
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.contrib.auth.password_validation import validate_password
from django.db import models
import sys,inspect,re

maxDigits = 30
maxDecimals = 18

#User Model

class User(AbstractUser):
	email = models.EmailField(
		verbose_name='email address',
		max_length=255,
		unique=True,
		blank=True
	)

	def save(self, *args, **kwargs):
		self.email = self.username
		super().save(*args,**kwargs)

@receiver(pre_save, sender=User)
def validate_user(sender, instance, raw, using, **kwargs):
	if not instance.is_superuser:
		validate_password(instance.password,instance)
		instance.set_password(instance.password)
	if not raw:
		instance.full_clean()

@receiver(post_save, sender=User)
def user_post_save(sender, instance, created, **kwargs):
	if created:
		#create Portfolio and Token for each new user
		Portfolio.objects.create(user=instance)
		Token.objects.create(user=instance)

#Coin Model

class Coin(models.Model):
	name = models.CharField(max_length=30, unique=True, validators=[MinLengthValidator(2)])
	ticker = models.CharField(max_length=10, validators=[MinLengthValidator(2)])
	def __str__(self):
		return "{} ({})".format(self.name,self.ticker)

@receiver(pre_save, sender=Coin)
def validate_coin(sender, instance, raw, using, **kwargs):
	if not raw:
		instance.full_clean()


#Portfolio Model

class Portfolio(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE)
	def __str__(self):
		return "{}'s Portfolio".format(self.user.__str__().capitalize())

@receiver(pre_save, sender=Portfolio)
def validate_portfolio(sender, instance, raw, using, **kwargs):
	if not raw:
		instance.full_clean()


#CoinHolding Model

def validate_quantity(quantity):
	if quantity <= 0:
		raise ValidationError("quantity must be greater than 0.")

class CoinHolding(models.Model):
	coin = models.ForeignKey(Coin,on_delete=models.CASCADE)
	quantity = models.DecimalField(max_digits=maxDigits, decimal_places=18, validators=[validate_quantity])
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	portfolio = models.ForeignKey(Portfolio,on_delete=models.CASCADE,blank=True,null=True)

	class Meta:
		unique_together = (("coin","user"),)

	def __str__(self):
		return "{}: {:,.8f} {}".format(self.user.__str__().capitalize(),self.quantity,self.coin)

@receiver(pre_save, sender=CoinHolding)
def validate_coinholding(sender, instance, raw, using, **kwargs):
	if not raw:
		instance.full_clean()

@receiver(post_save, sender=CoinHolding)
def set_portfolio_for_coin_holding(sender, instance, created, **kwargs):
	if created:
		instance.portfolio = instance.user.portfolio
		instance.save()


#CoinPrice Model

class CoinPrice(models.Model):
	coin = models.ForeignKey(Coin,on_delete=models.CASCADE)
	price = models.DecimalField(max_digits=13,decimal_places=6)
	date = models.DateField()
	time = models.TimeField()

	def __str__(self):
		return "{} {} - {} -  ${:,.2f}".format(self.date,self.time,self.coin,self.price)


@receiver(pre_save, sender=CoinPrice)
def validate_coinprice(sender, instance, raw, using, **kwargs):
	if not raw:
		instance.full_clean()


#Trade Model

class Trade(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	date = models.DateField()
	time = models.TimeField()
	exchange = models.CharField(max_length=30)
	buyCoin = models.ForeignKey(Coin,on_delete=models.CASCADE, related_name='BuyTrades')
	sellCoin = models.ForeignKey(Coin,on_delete=models.CASCADE, related_name='SellTrades')
	buyPrice = models.ForeignKey(CoinPrice,on_delete=models.CASCADE, related_name='+')
	sellPrice = models.ForeignKey(CoinPrice,on_delete=models.CASCADE, related_name='+')
	buyQuantity = models.DecimalField(max_digits=maxDigits,decimal_places=maxDecimals)
	sellQuantity = models.DecimalField(max_digits=maxDigits,decimal_places=maxDecimals)

@receiver(pre_save, sender=Trade)
def validate_trade(sender, instance, raw, using, **kwargs):
	if not raw:
		instance.full_clean()


#Transfer Model

class Transfer(models.Model):

	transferTypeChoices = (
		("D","Deposit"),
		("W","Withdrawal"),
		("P","Purchase"),
		("S","Sale")
	)

	user = models.ForeignKey(User,on_delete=models.CASCADE)
	date = models.DateField()
	time = models.TimeField()
	transferType = models.CharField(choices=transferTypeChoices, max_length=10)
	exchange = models.CharField(max_length=30)
	coin = models.ForeignKey(Coin,on_delete=models.CASCADE)
	price = models.ForeignKey(CoinPrice,on_delete=models.CASCADE)
	quantity = models.DecimalField(max_digits=maxDigits,decimal_places=maxDecimals)
	fromAddress = models.CharField(max_length=100)
	toAddress = models.CharField(max_length=100)

@receiver(pre_save, sender=Transfer)
def validate_transfer(sender, instance, raw, using, **kwargs):
	if not raw:
		instance.full_clean()

modelClassNames = [
	x[0] for x in filter(
		lambda x: 
			__name__ in str(x[1]) and 
			inspect.isclass(x[1]) and 
			'Validator' not in str(x[1]),
		inspect.getmembers(sys.modules[__name__])
	)
]