from rest_framework.test import APIClient, APITestCase
from rest_framework.authtoken.models import Token
from rest_framework import status
from restapiapp.models import User
from django.urls import reverse

class UserAPITests(APITestCase):
	@classmethod
	def setUpTestData(cls):
		User.objects.create(
			username='user1@gmail.com',
			password='Password_123'
		)

	def test_user_create(self):
		url = reverse('user-list')
		data = {
			"username": "user2@gmail.com",
			"password": "Password_123"
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code,201)		
		self.assertEqual(User.objects.count(), 2)
		self.assertEqual(User.objects.all()[1].username, 'user2@gmail.com')

	def test_user_can_get_token(self):
		url = reverse('get_auth_token')
		data = {
			'username': 'user1@gmail.com',
			'password': 'Password_123'
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code,200)

	def test_user_correct_token(self):
		user = User.objects.get(username='user1@gmail.com')
		token = Token.objects.get(user=user)
		url = reverse('get_auth_token')
		data = {
			'username': 'user1@gmail.com',
			'password': 'Password_123'
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.data["token"],token.key)

	def test_new_user_can_get_token(self):
		#create user
		url = reverse('user-list')
		data = {
			"username": "user2@gmail.com",
			"password": "Password_123"
		}
		self.client.post(url, data, format='json')		
		#get user
		url = reverse('get_auth_token')
		data = {
			'username': 'user2@gmail.com',
			'password': 'Password_123'
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code,200)

	def test_create_superuser(self):
		url = reverse('user-list')
		data = {
			'username': 'superuser@gmail.com',
			'password': 'Password_123',
			'is_admin': True
		}
		response = self.client.post(url, data, format='json')	
		self.assertEqual(response.status_code,201)		
		self.assertEqual(User.objects.count(), 2)
		self.assertEqual(User.objects.all()[1].username, 'superuser@gmail.com')

	def test_superuser_can_get_token(self):
		User.objects.create_superuser(
			username='superuser@gmail.com',
			email='superuser@gmail.com',
			password='Password_123'
		)
		#get user
		url = reverse('get_auth_token')
		data = {
			'username': 'superuser@gmail.com',
			'password': 'Password_123'
		}
		response = self.client.post(url, data, format='json')
		self.assertEqual(response.status_code,200)
