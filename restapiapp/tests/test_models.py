from django.test import TestCase
from restapiapp.models import *
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.utils.timezone import now
from decimal import Decimal

class UserModelTest(TestCase):
	expected_field_names = [
	"_state","id","password","last_login","username",
	"is_superuser","first_name","last_name","username",
	"email", "is_staff","is_active","date_joined"
	]

	@classmethod
	def setUpTestData(cls):
		User.objects.create(username="testuser1@gmail.com", password="Password_123")
	
	def test_field_names(self):
		user = User.objects.all()[0]
		actual_field_names = set(vars(user))
		for expected_field_name in self.expected_field_names:
			self.assertIn(expected_field_name,actual_field_names)

	def test_no_unwanted_fields(self):
		user = User.objects.all()[0]
		actual_field_names = vars(user)
		expected_field_name_set = set(self.expected_field_names)
		for actual_field_name in actual_field_names:
			self.assertIn(actual_field_name,expected_field_name_set)

	def test_field_values(self):
		user = User.objects.all()[0]
		expected_field_values = {"username":"testuser1@gmail.com", "portfolio":Portfolio.objects.all()[0]}
		for field_name in expected_field_values:
			self.assertEquals(expected_field_values[field_name],getattr(user,field_name))

	def test_create_valid_user(self):
		User.objects.create(username="testuser2@gmail.com", password="Password_123")

	def test_create_superuser(self):
		User.objects.create_superuser(username="testuser2@gmail.com", email="testuser2@gmail.com", password="Password_123")

	#Field: username
	def test_username_duplicate(self):
		createUserWithDuplicateUsername = lambda: User.objects.create(username="testuser1@gmail.com", password="Password_123")
		self.assertRaisesRegex(ValidationError,"username already exists",createUserWithDuplicateUsername)

	def test_username_too_long(self):
		createUserWithLongUsername = lambda: User.objects.create(username="AVeryIncrediblyLongUsernameThatIsTooLong"*10+"@gmail.com", password="Password_123")
		self.assertRaisesRegex(ValidationError,"username.*has at most",createUserWithLongUsername)

	def test_username_empty(self):
		createUserWithEmptyUsername = lambda: User.objects.create(username="", password="Password_123")
		self.assertRaisesRegex(ValidationError,"username.*This field cannot be blank",createUserWithEmptyUsername)

	def test_username_missing(self):
		createUserWithMissingUsername = lambda: User.objects.create(password="Password_123")
		self.assertRaisesRegex(ValidationError,"username.*This field cannot be blank",createUserWithMissingUsername)

	def test_username_missing_prefix(self):
		createUserWithInvalidUsername = lambda: User.objects.create(username="@gmail.com", password="Password_123")
		self.assertRaisesRegex(ValidationError,"Enter a valid email address",createUserWithInvalidUsername)

	def test_username_missing_suffix(self):
		createUserWithInvalidUsername = lambda: User.objects.create(username="UnusedUsername@gmail", password="Password_123")
		self.assertRaisesRegex(ValidationError,"Enter a valid email address",createUserWithInvalidUsername)

	def test_username_missing_at_sign(self):
		createUserWithInvalidUsername = lambda: User.objects.create(username="UnusedUsernamegmail.com", password="Password_123")
		self.assertRaisesRegex(ValidationError,"Enter a valid email address",createUserWithInvalidUsername)

	#Field: password
	def test_password_too_short(self):
		createUserWithShortPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password="2_Sh0rT")
		self.assertRaisesRegex(ValidationError,"This password is too short. It must contain at least 8 characters",createUserWithShortPassword)

	def test_password_empty(self):
		createUserWithEmptyPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password="")
		self.assertRaisesRegex(ValidationError,"It must contain at least 8 characters",createUserWithEmptyPassword)

	def test_password_missing(self):
		createUserWithMissingPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com")
		self.assertRaisesRegex(ValidationError,"It must contain at least 8 characters",createUserWithMissingPassword)

	def test_password_no_uppercase(self):
		createUserWithInvalidPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password="abcdefghijk_123")
		self.assertRaisesRegex(ValidationError,"Password must contain at least one uppercase character",createUserWithInvalidPassword)

	def test_password_no_lowercase(self):
		createUserWithInvalidPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password="ABCDEFGHIJK_123")
		self.assertRaisesRegex(ValidationError,"Password must contain at least one lowercase character",createUserWithInvalidPassword)

	def test_password_no_special_characters(self):
		createUserWithInvalidPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password="ABCDEFghijk123")
		self.assertRaisesRegex(ValidationError,"Password must contain at least one special character",createUserWithInvalidPassword)

	def test_password_no_numbers(self):
		createUserWithInvalidPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password="ABCDEF_ghijk")
		self.assertRaisesRegex(ValidationError,"Password must contain at least one number",createUserWithInvalidPassword)

	def test_password_contains_invalid_characters(self):
		createUserWithInvalidPassword = lambda: User.objects.create(username="UnusedEmail@gmail.com", password=r"ABCDEFGH*IJKabcd_123")
		self.assertRaisesRegex(ValidationError,"Password must not contain invalid characters",createUserWithInvalidPassword)

	def test_password_is_hashed(self):
		user = User.objects.create(username="UnusedEmail@gmail.com", password="Password_123")
		self.assertNotEqual(user.password,"Password_123")

class CoinModelTest(TestCase):
	
	expected_field_names = ["_state","id","name","ticker"]

	@classmethod
	def setUpTestData(cls):
		Coin.objects.create(name="Bitcoin",ticker="BTC")

	def test_field_names(self):
		coin = Coin.objects.all()[0]
		actual_field_names = set(vars(coin))
		for expected_field_name in self.expected_field_names:
			self.assertIn(expected_field_name,actual_field_names)

	def test_no_unwanted_fields(self):
		coin = Coin.objects.all()[0]
		expected_field_names_set = set(self.expected_field_names)
		actual_field_names = vars(coin)
		for actual_field_name in actual_field_names:
			self.assertIn(actual_field_name,expected_field_names_set)

	def test_field_values(self):
		coin = Coin.objects.all()[0]
		expected_field_values = {"name":"Bitcoin", "ticker":"BTC"}
		for field_name in expected_field_values:
			self.assertEquals(expected_field_values[field_name],getattr(coin,field_name))

	#Field: Name
	def test_name_duplicate(self):
		createCoinWithDuplicateName = lambda: Coin.objects.create(name="Bitcoin",ticker="ANYTHING")
		self.assertRaisesRegex(ValidationError,"Coin with this Name already exists",createCoinWithDuplicateName)

	def test_name_too_long(self):
		createCoinWithLongName = lambda: Coin.objects.create(name="The name of this coin is way too long", ticker="ANYTHING")
		self.assertRaisesRegex(ValidationError,"name.*has at most",createCoinWithLongName)

	def test_name_too_short(self):
		createCoinWithShortName = lambda: Coin.objects.create(name="B",ticker="ANYTHING")
		self.assertRaisesRegex(ValidationError,"name.*has at least",createCoinWithShortName)

	def test_name_empty(self):
		createCoinWithEmptyName = lambda: Coin.objects.create(name="",ticker="ANYTHING")
		self.assertRaisesRegex(ValidationError,"name.*This field cannot be blank",createCoinWithEmptyName)

	def test_name_missing(self):
		createCoinWithMissingName = lambda: Coin.objects.create(ticker="ANYTHING")
		self.assertRaisesRegex(ValidationError,"name.*This field cannot be blank",createCoinWithMissingName)

	#Field: Ticker
	def test_ticker_too_long(self):
		createCoinWithLongTicker = lambda: Coin.objects.create(name="ANYTHING", ticker="This ticker is way too long")
		self.assertRaisesRegex(ValidationError,"ticker.*has at most",createCoinWithLongTicker)

	def test_ticker_too_short(self):
		createCoinWithShortTicker = lambda: Coin.objects.create(name="ANYTHING", ticker="B")
		self.assertRaisesRegex(ValidationError,"ticker.*has at least",createCoinWithShortTicker)

	def test_ticker_empty(self):
		createCoinWithEmptyTicker = lambda: Coin.objects.create(name="ANYTHING", ticker="")
		self.assertRaisesRegex(ValidationError,"ticker.*This field cannot be blank",createCoinWithEmptyTicker)

	def test_ticker_missing(self):
		createCoinWithMissingTicker = lambda: Coin.objects.create(name="ANYTHING")
		self.assertRaisesRegex(ValidationError,"ticker.*This field cannot be blank",createCoinWithMissingTicker)

class CoinHoldingModelTest(TestCase):
	expected_field_names = ['_state', 'id', 'coin_id', 'quantity', 'user_id', 'portfolio_id']

	@classmethod
	def setUpTestData(cls):
		user = User.objects.create(username="testuser1@gmail.com",password="Password_123")
		coin = Coin.objects.create(name="Bitcoin",ticker="BTC")
		coin2 = Coin.objects.create(name="Ethereum",ticker="ETH")
		CoinHolding.objects.create(coin=coin,quantity=5,user=user)

	def test_field_names(self):
		coinholding = CoinHolding.objects.all()[0]
		actual_field_names = set(vars(coinholding))
		for expected_field_name in self.expected_field_names:
			self.assertIn(expected_field_name,actual_field_names)

	def test_no_unwanted_fields(self):
		coinholding = CoinHolding.objects.all()[0]
		actual_field_names = vars(coinholding)
		expected_field_name_set = set(self.expected_field_names)
		for actual_field_name in actual_field_names:
			self.assertIn(actual_field_name,expected_field_name_set)

	def test_field_values(self):
		coinholding = CoinHolding.objects.all()[0]
		expected_field_values = {"id":1, "coin_id":1, "quantity":5, "user_id":1, "portfolio_id":1}
		for field_name in expected_field_values:
			self.assertEquals(expected_field_values[field_name],getattr(coinholding,field_name))

	#Field: coin
	def test_coinholding_duplicate_coin_for_user(self):
		user = User.objects.all()[0]
		coin = Coin.objects.all()[0]
		createCoinHoldingDuplicateCoinForUser = lambda: CoinHolding.objects.create(coin=coin,quantity=10,user=user)	
		self.assertRaisesRegex(ValidationError,"Coin holding with this Coin and User already exists",createCoinHoldingDuplicateCoinForUser)

	def test_quantity_negative(self):
		user = User.objects.all()[0]
		coin = Coin.objects.all()[1]
		createCoinHoldingQuantityNegative = lambda: CoinHolding.objects.create(coin=coin,quantity=-10,user=user)	
		self.assertRaisesRegex(ValidationError,"quantity must be greater than 0",createCoinHoldingQuantityNegative)

	def test_quantity_zero(self):
		user = User.objects.all()[0]
		coin = Coin.objects.all()[1]
		createCoinHoldingQuantityZero = lambda: CoinHolding.objects.create(coin=coin,quantity=0,user=user)	
		self.assertRaisesRegex(ValidationError,"quantity must be greater than 0",createCoinHoldingQuantityZero)

	def test_quantity_string(self):
		user = User.objects.all()[0]
		coin = Coin.objects.all()[1]
		createCoinHoldingQuantityString = lambda: CoinHolding.objects.create(coin=coin,quantity="zero",user=user)	
		self.assertRaisesRegex(ValidationError,"quantity.*value must be a decimal number",createCoinHoldingQuantityString)

	def test_quantity_missing(self):
		user = User.objects.all()[0]
		coin = Coin.objects.all()[1]
		createCoinHoldingQuantityMissing = lambda: CoinHolding.objects.create(coin=coin,user=user)	
		self.assertRaisesRegex(ValidationError,"quantity.*This field cannot be null",createCoinHoldingQuantityMissing)

	def test_user_string(self):
		user = "User"
		coin = Coin.objects.all()[1]
		createCoinHoldingUserString = lambda: CoinHolding.objects.create(coin=coin,quantity=5,user=user)	
		self.assertRaisesRegex(ValueError,"must be a \"User\" instance",createCoinHoldingUserString)

	def test_user_int(self):
		user = 5
		coin = Coin.objects.all()[1]
		createCoinHoldingUserString = lambda: CoinHolding.objects.create(coin=coin,quantity=5,user=user)	
		self.assertRaisesRegex(ValueError,"must be a \"User\" instance",createCoinHoldingUserString)

	def test_user_missing(self):
		coin = Coin.objects.all()[1]
		createCoinHoldingUserString = lambda: CoinHolding.objects.create(coin=coin,quantity=5)	
		self.assertRaisesRegex(ValidationError,"user.*This field cannot be null",createCoinHoldingUserString)

	def test_coin_string(self):
		user = User.objects.all()[0]
		coin = "Coin"
		createCoinHoldingQuantityString = lambda: CoinHolding.objects.create(coin=coin,quantity=5,user=user)	
		self.assertRaisesRegex(ValueError,"\"CoinHolding.coin\" must be a \"Coin\" instance",createCoinHoldingQuantityString)

	def test_coin_int(self):
		user = User.objects.all()[0]
		coin = 5
		createCoinHoldingQuantityString = lambda: CoinHolding.objects.create(coin=coin,quantity=5,user=user)	
		self.assertRaisesRegex(ValueError,"\"CoinHolding.coin\" must be a \"Coin\" instance",createCoinHoldingQuantityString)

	def test_coin_missing(self):
		user = User.objects.all()[0]
		createCoinHoldingQuantityString = lambda: CoinHolding.objects.create(quantity="0",user=user)	
		self.assertRaisesRegex(ValidationError,"coin.*This field cannot be null",createCoinHoldingQuantityString)

class PortfolioModelTest(TestCase):
	expected_field_names = ["_state","id","user_id"]

	@classmethod
	def setUpTestData(cls):
		user = User.objects.create(username="testuser1@gmail.com",password="Password_123")
		#Portfolio is created in post_save user function		
		coin = Coin.objects.create(name="Bitcoin",ticker="BTC")
		CoinHolding.objects.create(coin=coin,quantity=5,user=user)		

	def test_field_names(self):
		portfolio = Portfolio.objects.all()[0]
		actual_field_names = set(vars(portfolio))
		for expected_field_name in self.expected_field_names:
			self.assertIn(expected_field_name,actual_field_names)

	def test_no_unwanted_fields(self):
		portfolio = Portfolio.objects.all()[0]
		expected_field_names_set = set(self.expected_field_names)
		actual_field_names = vars(portfolio)
		for actual_field_name in actual_field_names:
			self.assertIn(actual_field_name,expected_field_names_set)

	def test_field_values(self):
		portfolio = Portfolio.objects.all()[0]
		user = User.objects.all()[0]
		expected_field_values = {"user":user, "coinholding_set":CoinHolding.objects.filter(portfolio_id=portfolio.id)}
		for field_name in expected_field_values:
			if "set" not in field_name:
				self.assertEquals(expected_field_values[field_name],getattr(portfolio,field_name))
			else:
				self.assertEquals(expected_field_values[field_name].in_bulk(),getattr(portfolio,field_name).in_bulk())

	def test_portfolio_manual_create(self):
		createPortfolio = lambda: Portfolio.objects.create(user=User.objects.all()[0])
		self.assertRaisesRegex(ValidationError,"user.*Portfolio with this User already exists",createPortfolio)

	#Field: user
	def test_user_delete_should_delete_portfolio(self):
		User.objects.all()[0].delete()
		getPortfolio = lambda: Portfolio.objects.all()[0]
		self.assertRaisesRegex(IndexError,"list index out of range",getPortfolio)

	def test_user_missing(self):
		createPortfolio = lambda: Portfolio.objects.create()
		self.assertRaisesRegex(ValidationError,"user.*This field cannot be null",createPortfolio)

class CoinPriceModelTest(TestCase):
	datetime = now()
	date = datetime.date()
	time = datetime.time()

	@classmethod
	def setUpTestData(cls):
		coin = Coin.objects.create(name="Bitcoin",ticker="BTC")
		coinPrice = CoinPrice.objects.create(coin=coin,price="8532.0155",date=cls.date, time=cls.time)

	#Field: coin
	def test_coin_field_exists(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertIn("coin_id",vars(coinPrice)) #coin_id used because coin is a ForeignKey
		self.assertIn("coin",dir(coinPrice))

	def test_coin_value_correct(self):
		expectedId = Coin.objects.all()[0].id
		coinPrice = CoinPrice.objects.all()[0]
		self.assertEquals(expectedId,coinPrice.coin.id)

	def test_coin_missing(self):
		createCoinPriceWithMissingCoin = lambda: CoinPrice.objects.create(price="334.54",date=self.__class__.date,time=self.__class__.time)
		self.assertRaisesRegex(ValidationError,"coin.*This field cannot be null",createCoinPriceWithMissingCoin)

	#Field: price
	def test_price_field_exists(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertIn("price",vars(coinPrice))

	def test_price_value_correct(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertEquals(Decimal("8532.0155"),coinPrice.price)

	def test_price_missing(self):
		coin = Coin.objects.create(name="Ethereum",ticker="ETH")
		createCoinPriceWithMissingPrice = lambda: CoinPrice.objects.create(coin=coin,date=self.__class__.date,time=self.__class__.time)
		self.assertRaisesRegex(ValidationError,"price.*This field cannot be null",createCoinPriceWithMissingPrice)

	#Field: date
	def test_date_field_exists(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertIn("date",vars(coinPrice))

	def test_date_value_correct(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertEquals(self.__class__.date,coinPrice.date)

	def test_date_missing(self):
		coin = Coin.objects.create(name="Ethereum",ticker="ETH")
		createCoinPriceWithMissingDate = lambda: CoinPrice.objects.create(price="334.54",coin=coin)
		self.assertRaisesRegex(ValidationError,"date.*This field cannot be null",createCoinPriceWithMissingDate)

	#Field: time
	def test_time_field_exists(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertIn("time",vars(coinPrice))

	def test_time_value_correct(self):
		coinPrice = CoinPrice.objects.all()[0]
		self.assertEquals(self.__class__.time,coinPrice.time)

	def test_time_missing(self):
		coin = Coin.objects.create(name="Ethereum",ticker="ETH")
		createCoinPriceWithMissingTime = lambda: CoinPrice.objects.create(price="334.54",coin=coin)
		self.assertRaisesRegex(ValidationError,"time.*This field cannot be null",createCoinPriceWithMissingTime)

class TradeModelTest(TestCase):
	datetime = now()
	date = datetime.date()
	time = datetime.time()

	@classmethod
	def setUpTestData(cls):
		buyCoin = Coin.objects.create(name="Bitcoin",ticker="BTC")
		sellCoin = Coin.objects.create(name="Ethereum",ticker="ETH")
		Trade.objects.create(
			user = User.objects.create(username="testuser1@gmail.com",password="Password_123"),
			date = cls.date,
			time = cls.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=cls.date,time=cls.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=cls.date,time=cls.time)
		)

	#Field: user

	def test_user_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("user_id",vars(trade))
		self.assertIn("user",dir(trade))
	
	def test_user_value_correct(self):
		expectedId = User.objects.all()[0].id;
		trade = Trade.objects.all()[0]
		self.assertEquals(expectedId,trade.user.id)

	def test_user_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingUser = lambda: Trade.objects.create(
			date = self.__class__.date,
			time = self.__class__.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"user.*This field cannot be null",createTradeWithMissingUser)


	#Field: date

	def test_date_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("date",vars(trade))
	
	def test_date_value_correct(self):
		trade = Trade.objects.all()[0]
		self.assertEquals(self.__class__.date,trade.date)

	def test_date_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingDate = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			time = self.__class__.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"date.*This field cannot be null",createTradeWithMissingDate)

	#Field: time

	def test_time_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("time",vars(trade))
	
	def test_time_value_correct(self):
		trade = Trade.objects.all()[0]
		self.assertEquals(self.__class__.time,trade.time)

	def test_time_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingTime = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),			
			date = self.__class__.date,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"time.*This field cannot be null",createTradeWithMissingTime)


	#Field: exchange

	def test_exchange_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("exchange",vars(trade))
	
	def test_exchange_value_correct(self):
		trade = Trade.objects.all()[0]
		self.assertEquals("Binance",trade.exchange)

	def test_exchange_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingExchange = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"exchange.*This field cannot be blank",createTradeWithMissingExchange)


	#Field: buyCoin

	def test_buyCoin_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("buyCoin_id",vars(trade))
		self.assertIn("buyCoin",dir(trade))
	
	def test_buyCoin_value_correct(self):
		expectedId = Coin.objects.all()[0].id
		trade = Trade.objects.all()[0]
		self.assertEquals(expectedId,trade.buyCoin.id)

	def test_buyCoin_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingBuyCoin = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"buyCoin.*This field cannot be null",createTradeWithMissingBuyCoin)
	

	#Field: sellCoin

	def test_sellCoin_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("sellCoin_id",vars(trade))
		self.assertIn("sellCoin",dir(trade))
	
	def test_sellCoin_value_correct(self):
		expectedId = Coin.objects.all()[1].id
		trade = Trade.objects.all()[0]
		self.assertEquals(expectedId,trade.sellCoin.id)

	def test_sellCoin_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingSellCoin = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"sellCoin.*This field cannot be null",createTradeWithMissingSellCoin)


	#Field: buyPrice

	def test_buyPrice_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("buyPrice_id",vars(trade))
		self.assertIn("buyPrice",dir(trade))
	
	def test_buyPrice_value_correct(self):
		expectedId = CoinPrice.objects.all()[0].id
		trade = Trade.objects.all()[0]
		self.assertEquals(expectedId,trade.buyPrice.id)

	def test_buyPrice_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingBuyPrice = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"buyPrice.*This field cannot be null",createTradeWithMissingBuyPrice)


	#Field: sellPrice

	def test_sellPrice_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("sellPrice_id",vars(trade))
		self.assertIn("sellPrice",dir(trade))
	
	def test_sellPrice_value_correct(self):
		expectedId = CoinPrice.objects.all()[1].id
		trade = Trade.objects.all()[0]
		self.assertEquals(expectedId,trade.sellPrice.id)

	def test_sellPrice_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingSellPrice = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0"
		)
		self.assertRaisesRegex(ValidationError,"sellPrice.*This field cannot be null",createTradeWithMissingSellPrice)


	#Field: buyQuantity

	def test_buyQuantity_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("buyQuantity",vars(trade))
	
	def test_buyQuantity_value_correct(self):
		trade = Trade.objects.all()[0]
		self.assertEquals(Decimal("1.0"),trade.buyQuantity)

	def test_buyQuantity_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingUser = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellQuantity="10.0",
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"buyQuantity.*This field cannot be null",createTradeWithMissingUser)


	#Field: buyQuantity

	def test_sellQuantity_field_exists(self):
		trade = Trade.objects.all()[0]
		self.assertIn("sellQuantity",vars(trade))
	
	def test_sellQuantity_value_correct(self):
		trade = Trade.objects.all()[0]
		self.assertEquals(Decimal("10.0"),trade.sellQuantity)

	def test_sellQuantity_missing(self):
		buyCoin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		sellCoin = Coin.objects.create(name="NotEthereum",ticker="NETH")
		createTradeWithMissingUser = lambda: Trade.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,
			time = self.__class__.time,
			exchange="Binance",
			buyCoin=buyCoin,
			buyQuantity="1.0",
			buyPrice=CoinPrice.objects.create(coin=buyCoin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			sellCoin=sellCoin,
			sellPrice=CoinPrice.objects.create(coin=sellCoin,price="1000.01",date=self.__class__.date,time=self.__class__.time)
		)
		self.assertRaisesRegex(ValidationError,"sellQuantity.*This field cannot be null",createTradeWithMissingUser)

class TransferModelTest(TestCase):
	datetime = now()
	date = datetime.date()
	time = datetime.time()

	@classmethod
	def setUpTestData(cls):
		coin = Coin.objects.create(name="Bitcoin",ticker="BTC")
		Transfer.objects.create(
			user = User.objects.create(username="testuser1@gmail.com",password="Password_123"),
			date = cls.date,
			time = cls.time,
			transferType="D",
			exchange="Binance",
			coin=coin,
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=cls.date,time=cls.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",		
		)


	#Field: user

	def test_user_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("user_id",vars(transfer))
		self.assertIn("user",dir(transfer))
	
	def test_user_value_correct(self):
		expectedId = User.objects.all()[0].id;
		transfer = Transfer.objects.all()[0]
		self.assertEquals(expectedId,transfer.user.id)

	def test_user_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingUser = lambda: Transfer.objects.create(
			date = self.__class__.date,
			time = self.__class__.time,
			transferType = "W",
			exchange="Binance",
			coin=coin,
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"user.*This field cannot be null",createTransferWithMissingUser)


	#Field: date

	def test_date_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("date",vars(transfer))
	
	def test_date_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals(self.__class__.date,transfer.date)

	def test_date_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingDate = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			time = self.__class__.time,
			transferType = "W",
			exchange="Binance",
			coin=coin,
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"date.*This field cannot be null",createTransferWithMissingDate)


	#Field: time

	def test_time_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("time",vars(transfer))
	
	def test_time_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals(self.__class__.time,transfer.time)

	def test_time_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingTime = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			transferType = "W",
			exchange="Binance",
			coin=coin,
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"time.*This field cannot be null",createTransferWithMissingTime)


	#Field: transferType

	def test_transferType_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("transferType",vars(transfer))
	
	def test_transferType_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals("D",transfer.transferType)

	def test_transferType_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingTransfer = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			exchange="Binance",
			coin=coin,
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"transferType.*This field cannot be blank",createTransferWithMissingTransfer)


	#Field: exchange

	def test_exchange_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("exchange",vars(transfer))
	
	def test_exchange_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals("Binance",transfer.exchange)

	def test_exchange_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingExchange = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			transferType = "W",
			coin=coin,
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"exchange.*This field cannot be blank",createTransferWithMissingExchange)


	#Field: coin

	def test_coin_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("coin_id",vars(transfer))
		self.assertIn("coin",dir(transfer))

	def test_coin_value_correct(self):
		expectedId = Coin.objects.all()[0].id
		transfer = Transfer.objects.all()[0]
		self.assertEquals(expectedId,transfer.coin.id)

	def test_coin_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingCoin = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			exchange="Binance",
			transferType = "W",
			quantity="1.0",
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"coin.*This field cannot be null",createTransferWithMissingCoin)


	#Field: price
	def test_price_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("price_id",vars(transfer))
		self.assertIn("price",dir(transfer))

	def test_price_value_correct(self):
		expectedId = CoinPrice.objects.all()[0].id
		transfer = Transfer.objects.all()[0]
		self.assertEquals(expectedId,transfer.price.id)

	def test_price_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingPrice = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			exchange="Binance",
			transferType = "W",
			coin=coin,
			quantity="1.0",
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"price.*This field cannot be null",createTransferWithMissingPrice)


	#Field: quantity
	def test_quantity_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("quantity",vars(transfer))

	def test_quantity_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals(Decimal("1.0"),transfer.quantity)

	def test_quantity_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingQuantity = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			exchange="Binance",
			transferType = "W",
			coin=coin,
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"quantity.*This field cannot be null",createTransferWithMissingQuantity)


	#Field: fromAddress
	def test_fromAddress_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("fromAddress",vars(transfer))

	def test_fromAddress_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals("0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",transfer.fromAddress)

	def test_fromAddress_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingFromAddress = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			exchange="Binance",
			transferType = "W",
			coin=coin,
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			quantity="1.0",
			toAddress="0xAE6dB0D6473F20d4bD61960614E5d122658255d9",
		)
		self.assertRaisesRegex(ValidationError,"fromAddress.*This field cannot be blank",createTransferWithMissingFromAddress)


	#Field: toAddress
	def test_toAddress_field_exists(self):
		transfer = Transfer.objects.all()[0]
		self.assertIn("toAddress",vars(transfer))

	def test_toAddress_value_correct(self):
		transfer = Transfer.objects.all()[0]
		self.assertEquals("0xAE6dB0D6473F20d4bD61960614E5d122658255d9",transfer.toAddress)

	def test_toAddress_missing(self):
		coin = Coin.objects.create(name="NotBitcoin",ticker="NBTC")
		createTransferWithMissingToAddress = lambda: Transfer.objects.create(
			user = User.objects.create(username="testuser2@gmail.com",password="Password_123"),
			date = self.__class__.date,			
			time = self.__class__.time,
			exchange="Binance",
			transferType = "W",
			coin=coin,
			price=CoinPrice.objects.create(coin=coin,price="10000.02",date=self.__class__.date,time=self.__class__.time),
			quantity="1.0",
			fromAddress="0x50e9B7A022e4654faB9a35f796c0e74ece06C4bE",
		)
		self.assertRaisesRegex(ValidationError,"toAddress.*This field cannot be blank",createTransferWithMissingToAddress)
