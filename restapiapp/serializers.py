from rest_framework import serializers
from .models import *

#Basic Model Serializers

class CoinSerializer(serializers.ModelSerializer):
	class Meta:
		model = Coin
		fields = ('id','name','ticker')

class CoinHoldingSerializer(serializers.ModelSerializer):
	coin = CoinSerializer(read_only=True)

	class Meta:
		model = CoinHolding
		fields = ('coin', 'quantity')
		read_only_fields = ('portfolio',)

class CoinPriceSerializer(serializers.ModelSerializer):
	class Meta:
		model = CoinPrice
		fields = ('coin','price', 'date', 'time')
		depth = 1

class PortfolioSerializer(serializers.ModelSerializer):
	coinholding_set = CoinHoldingSerializer(many=True)

	class Meta:
		model = Portfolio
		fields = ('id','coinholding_set')

class UserSerializer(serializers.ModelSerializer):
	portfolio = PortfolioSerializer(read_only=True)

	class Meta:
		model = User
		fields = ('id','username','trade_set','transfer_set','portfolio','password')
		read_only_fields = ('trade_set','transfer_set','portfolio')
		extra_kwargs = {'password':{'write_only':True}}

class TradeSerializer(serializers.ModelSerializer):
	buyCoin = CoinSerializer()
	sellCoin = CoinSerializer()

	class Meta:
		model = Trade
		exclude = ('user',)

class TransferSerializer(serializers.ModelSerializer):
	class Meta:
		model = Transfer
		exclude = ('user',)
		depth = 1



#Specialized Model Serializers

class CoinPriceDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = CoinPrice
		fields = ('price', 'date', 'time')