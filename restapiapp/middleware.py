import json

class RequestResponseLoggingMiddleware:
	def __init__(self, get_response):
		self.get_response = get_response

	def __call__(self, request):		
		#LOGGING FOR REQUEST
		print("\nREQUEST\n\tURL: {}".format(request.get_raw_uri()))
		print("\tMETHOD: {}".format(request.method))
		print("\tHEADERS:\n\t{")
		for header in request.META:
			if any(map(lambda x: x in header, ["HTTP_","CONTENT"])):
				print("\t\t{}: {}".format(header,request.META[header]))
		print("\t}")
		if request.body:
			print("\tBODY:\n\t{")
			try:
				body = json.loads(request.body)
				for item in body:
					print("\t\t{}: {}".format(item,body[item]))
			except:
				print(request.body)
			print("\t}")

		response = self.get_response(request)
		responseAttributes = dir(response)

		#LOGGING FOR RESPONSE
		print("\nRESPONSE")
		print("\tHEADERS:\n\t{")
		for header in response._headers:
			print("\t\t{}: {},".format(header,response._headers[header]))
		print("\t}")
		if "data" in responseAttributes:
			print("\tBODY:\n\t{")
			for item in response.data:
				print("\t\t{}: {},".format(item,response.data[item]))		
			print("\t}")

		print("STATUS_CODE: {}".format(response.status_code))
		if "status_text" in responseAttributes:
			print("STATUS_TEXT: {}".format(response.status_text))
		return response