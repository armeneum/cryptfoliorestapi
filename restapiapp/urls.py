from rest_framework import routers
from .views import *
from .models import modelClassNames

router = routers.DefaultRouter()

""" Basic Endpoints """
router.register("coins",CoinViewSet)
router.register("coinholdings",CoinHoldingViewSet)
router.register("coinprices",CoinPriceViewSet)
#router.register("coinprices/{coinName}",CoinPriceViewSet)
router.register("portfolios",PortfolioViewSet)
router.register("trades",TradeViewSet)
router.register("transfers",TransferViewSet)
router.register("users",UserViewSet)

urlpatterns = router.urls